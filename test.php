    /**
     * Main query for building packages grid
     * @param int $groupId
     * @param string $token
     * @return object
     */
    public function getAllGroupPackagesBuilder(int $groupId = null, string $token = null) : object
    {
        if(isset($token)){
            $groupRow = DB::table('groups')->select('id')->where('token', $token)->first();
            $groupId = $groupRow->id;
        }

        $versionColumns = $groupId ? 'vp.id as version_id,  vp.name as version_name, vp.file as version_file,' : '';
        $versionGroup = $groupId ? 'vp.id' : null;


        $query = (new self)
            ->newQuery()
            ->select(DB::raw('packages.id as package_id, packages.id as package_id_additional, packages.id as package_id_additional2, packages.name as package_name, 
                    ' . $versionColumns . 'GROUP_CONCAT(DISTINCT cp.name SEPARATOR  ", ") as categories_name,
                    GROUP_CONCAT(DISTINCT gp.name SEPARATOR  ", ") as groups_name, GROUP_CONCAT(DISTINCT gp.id SEPARATOR  ", ") as groups_ids,
                     GROUP_CONCAT(DISTINCT pd.name SEPARATOR  ", ") as dependencies, lp.id as label_id, lp.name as label_name'))
            ->leftJoin('category_to_package as ctp', 'ctp.package_id', '=', 'packages.id')
            ->leftJoin('categories as cp', 'cp.id', '=', 'ctp.category_id')
            ->leftJoin('group_to_package as gtp', 'gtp.package_id', '=', 'packages.id')
            ->leftJoin('groups as gp', 'gp.id', '=', 'gtp.group_id')
            ->leftJoin('label_to_package as ltp', 'packages.id', '=', 'ltp.package_id')
            ->leftJoin('labels as lp', 'lp.id', '=', 'ltp.label_id')
            ->leftJoin('dependencies as d', 'd.id_package', '=', 'packages.id')
            ->leftJoin('packages as pd', 'pd.id', '=', 'd.id_dependency');
        if (isset($groupId)) {
            //We put this left joins here because for different group, there is different active version, that means we can't show them in All Packages page
            $query->leftJoin('version_to_group as gtv', function($join)
            {
                $join->on('gtv.id_package', '=', 'packages.id');
                $join->on('gtv.id_group','=', 'gp.id');
            });
            $query->leftJoin('versions as vp', 'vp.id', '=', 'gtv.id_version');
            $query->where('gp.id', '=', $groupId);
        } elseif (isset($token)) {
            $query->where('gp.token', '=', $token);
        }

        $query->groupBy('packages.id', 'lp.id',  $versionGroup);
        $query->orderBy('lp.id');
        
        return $query;
    }
    
    
    /**
     * Save new package with group
     * @param array $data
     * @param object $request
     * @param int $groupId
     * @return void
     */
    public function saveNewPackage(array $data, object $request, int $groupId) : void
    {

        $packageId = DB::table('packages')->insertGetId(['name' => htmlentities($data['name'])]);

        if(isset($data['categories'])){
            $this->saveNewPackageCategories($data['categories'], $packageId);
        }

        $this->saveNewPackageGroups([$groupId], $packageId);
        $this->saveNewPackageVersions($data, $packageId, $groupId);

        if(isset($data['label'])){
            DB::table('label_to_package')->insert(['label_id' => $data['label'],'package_id' => $packageId]);
        }

        if(isset($data['dependency'])){
            foreach($data['dependency'] as $dependency){
                DB::table('dependencies')->insert(['id_package' => $packageId,'id_dependency' => $dependency['id_dependency'], 'id_version' => $dependency['id_version'], 'indicator' => clearVarsHelper::removeEmoji($dependency['indicator'])]);
            }

        }
    }
    
    /**
     * Save versions for package
     * @param array $data
     * @param int $packageId
     * @param int|array $groupIds
     * @return void
     */
    public function saveNewPackageVersions(array $data, int $packageId, $groupIds) : void
    {
        for($i=0; $i<count($data['version']); $i++){
            $data['version'][$i]['id_package'] = $packageId;
            $data['version'][$i]['name'] = clearVarsHelper::removeEmoji($data['version'][$i]['name']);
            $data['version'][$i]['comment'] = clearVarsHelper::removeEmoji($data['version'][$i]['comment']);
            $data['version'][$i]['tag'] = clearVarsHelper::removeEmoji($data['version'][$i]['tag']);

            if(isset($data['version'][$i]['newFile'])){
                $filename = $this->saveOrUpdateFile($data['version'][$i]['newFile'], $packageId, $data['version'][$i]['file']);
                $data['version'][$i]['file'] = $filename;
                unset($data['version'][$i]['newFile']);
            }

            $insertedVersions[] = DB::table('versions')->insertGetId($data['version'][$i]);
        }

        $lastVersion = end($insertedVersions);
        if(is_array($groupIds)){
            foreach($groupIds as $groupId){
                DB::table('version_to_group')->insert(['id_group' => $groupId,'id_package' => $packageId, 'id_version' => $lastVersion]);
            }
        }else{
            DB::table('version_to_group')->insert(['id_group' => $groupIds,'id_package' => $packageId, 'id_version' => $lastVersion]);
        }
    }
    
    
    /**
     * Save uploaded package file
     * @param object $file
     * @param int $packageId
     * @param string $oldFilename
     * @return string
     */
    public function saveOrUpdateFile(object $file, int $packageId, string $oldFilename = null) : string 
    {

        if(isset($file)){
            if(isset($oldFilename)){
                $exists = Storage::disk('s3')->exists('/packages_files/' .  $oldFilename);
                if($exists){
                    Storage::disk('s3')->delete('/packages_files/' . $oldFilename);
                }
            }
            
            $filename = $file->getClientOriginalName();
            $filename = pathinfo($filename, PATHINFO_FILENAME) . '-id' . $packageId .  '.' . pathinfo($filename, PATHINFO_EXTENSION);
            Storage::disk('s3')->put('/packages_files/' . $filename, file_get_contents($file));

        }else {
            $filename = isset($oldFilename) ? $oldFilename : '' ;
        }

        return $filename;
    }