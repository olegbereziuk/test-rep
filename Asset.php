<?php

namespace App;


use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Intervention\Image\ImageManagerStatic as Image;

class Assets extends Model
{
    protected $table = 'models';
    protected $primaryKey = 'id';

    public function __construct()
    {


    }

    /**
     * Save new asset
     * @param object $request
     * @return void
     */
    public function saveAsset($request)
    {

        try {
            //save unity file to Amazon S3
            $file = $request->file('unity_file');
            $fileNameOriginal = $file->getClientOriginalName();
            $filenameShort = pathinfo($fileNameOriginal, PATHINFO_FILENAME);
            $fileExtension = pathinfo($fileNameOriginal, PATHINFO_EXTENSION);

            $fileName = $filenameShort . '_' . time() . '.' . $fileExtension;

            Storage::disk('s3')->putFileAs('assets', new File($file->getPathName()), $fileName);

            //save icon file to Amazon S3
            $icon = $request->file('icon');
            $icon_1 = $request->file('icon_1');
            $iconData = $this->iconSave($icon);
            $iconSecondData = $this->iconSave($icon_1);

            //save asset data to DB
            $id = DB::table('catalog')->insertGetId(['icon' => $iconData['iconName'], 'icon_1' => $iconSecondData['iconName'],'icon_min' => $iconData['iconNameMin'],'icon_min_1' => $iconSecondData['iconNameMin'], 'title' => $request->title, 'description' => $request->description, 'file_name' => $fileName, 'file_size' => round($file->getSize() * 0.0000010, 2)]);
            DB::table('catalog_categories')->insert(['id_catalog' => $id, 'id_category' => $request->category]);

            $this->saveTags($id, $request->tags);


            //save unity file to local storage folder, to extract it
            Storage::disk('local')->putFileAs('/', new File($file->getPathName()), $fileName);
            $phar = new \PharData(storage_path('app/' . $fileName));
            $phar->extractTo(storage_path('app/' . $fileName . '_dir'));


            //build folders tree and save previews to S3
            $files = $this->getDirContents(storage_path('app/' . $fileName . '_dir'));
            $foldersArr = [];
            for($i = 0; $i <= count($files) - 1; $i++){
                $currentPath = explode('/', $files[$i]);

                if(isset($files[$i - 1])){

                    $prevPath = explode('/', $files[$i - 1]);

                    if($currentPath[7] == $prevPath[7]){
                        if(isset($currentPath[8]) && $currentPath[8] == 'pathname') {
                            $content = file_get_contents($files[$i]);;
                            $foldersArr[ '/' . $content] = '/' . $content;


                            $previewPath = storage_path('app/' . $fileName . '_dir/' . $currentPath[7] . '/preview.png');
                            $previewMaxPath = storage_path('app/' . $fileName . '_dir/' . $currentPath[7] . '/asset');

                            if(file_exists($previewMaxPath) && is_array(getimagesize($previewMaxPath))){
                                $contentArr = explode('/', $content);
                                $contentLastEl = array_pop($contentArr);

                                $newPreviewName = $contentLastEl . '.png';
                                Storage::disk('s3')->putFileAs('previews/' . $id, new File($previewMaxPath), $newPreviewName);
                            }else{
                                if(file_exists($previewPath)){
                                    $contentArr = explode('/', $content);
                                    $contentLastEl = array_pop($contentArr);

                                    $newPreviewName = $contentLastEl . '.png';
                                    Storage::disk('s3')->putFileAs('previews/' . $id, new File($previewPath), $newPreviewName);

                                }
                            }

                        }
                    }
                }

            }

            $foldersTreeArr = $this->explodeTree($foldersArr, '/');
            DB::table('catalog')->where('id', $id)->update(['folders_tree' => $foldersTreeArr]);

            $this->delFoldersTree(storage_path('app/' . $fileName . '_dir'));
            unlink(storage_path('app/' . $fileName));
            return ['status' => 'success', 'msg' => 'Asset saved'];
        }catch (\Exception $e) {
            return ['status' => 'error', 'msg' =>  $e->getMessage()];
        }
    }


    public function iconSave($icon){
        if(!empty($icon)){
            $originalName = $icon->getClientOriginalName();
            $IconOriginalName = pathinfo($originalName, PATHINFO_FILENAME) . '_' . time();
            $extension = pathinfo($originalName, PATHINFO_EXTENSION);
            $iconName = $IconOriginalName  . '.' . $extension;
            $iconNameMin = $IconOriginalName . '-min.' . $extension;

            Storage::disk('s3')->putFileAs('icons', new File($icon->getPathName()), $iconName);

            $image_resize = Image::make($icon->getRealPath());
            $image_resize->resize(150, 150);
            $image_resize->save(public_path('images/' . $iconNameMin));

            Storage::disk('s3')->putFileAs('icons', new File(public_path('images/' . $iconNameMin)), $iconNameMin);
            unlink(public_path('images/' . $iconNameMin));

        }else{
            $iconName = 'no-image-1.png';
            $iconNameMin = 'no-image-1.png';
        }

        return ['iconName' => $iconName, 'iconNameMin' => $iconNameMin];
    }

    public function delFoldersTree($dir)
    {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delFoldersTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }


    public function explodeTree($array, $delimiter = '_', $baseval = false)
    {
        if(!is_array($array)) return false;
        $splitRE   = '/' . preg_quote($delimiter, '/') . '/';
        $returnArr = array();
        foreach ($array as $key => $val) {
            // Get parent parts and the current leaf
            $parts	= preg_split($splitRE, $key, -1, PREG_SPLIT_NO_EMPTY);
            $leafPart = array_pop($parts);

            // Build parent structure
            // Might be slow for really deep and large structures
            $parentArr = &$returnArr;
            foreach ($parts as $part) {
                if (!isset($parentArr[$part])) {
                    $parentArr[$part] = array();
                } elseif (!is_array($parentArr[$part])) {
                    if ($baseval) {
                        $parentArr[$part] = array('__base_val' => $parentArr[$part]);
                    } else {
                        $parentArr[$part] = array();
                    }
                }
                $parentArr = &$parentArr[$part];
            }

            // Add the final part to the structure
            if (empty($parentArr[$leafPart])) {
                $parentArr[$leafPart] = $val;
            } elseif ($baseval && is_array($parentArr[$leafPart])) {
                $parentArr[$leafPart]['__base_val'] = $val;
            }
        }
        return json_encode($returnArr);
    }


    public function getDirContents($dir, &$results = array()) {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
            }
        }

        return $results;
    }


    public static function recursiveFoldersDisplay($array, $level = 1){
        try{

        foreach($array as $key => $value){
            //If $value is an array.
            if(is_array($value)){
                if($level > 2){
                    $display = 'display:none';
                }else{
                    $display = 'display:block';
                }

                echo '<span style="' . $display . '"  class="folder level-' . $level . '" data-level="' . $level . '">' . str_repeat("&nbsp&nbsp&nbsp&nbsp", $level) .  '<i class="fas fa-folder"></i>&nbsp' . $key . '</span>';
                self::recursiveFoldersDisplay($value, $level + 1);
            } else{
                //It is not an array, so print it out.
                echo '<span style="display:none"  class="file level-' . $level . '" data-level="' . $level . '">' . str_repeat("&nbsp&nbsp&nbsp&nbsp", $level) . '<i class="fas fa-file-image"></i>&nbsp' .  $key, '</span>';
            }
        }
    }catch (\Exception $e) {
        return back()->with('error', 'Error while reading package ' . $e->getMessage());
        }
    }


    /**
     * Edit existing asset
     * @param object $request
     * @return void
     */
    public function editAsset($request)
    {
        try {
            $file = $request->file('unity_file');
            if(!empty($file)){
                $fileNameOriginal = $file->getClientOriginalName();
                $filenameShort = pathinfo($fileNameOriginal, PATHINFO_FILENAME);
                $fileExtension = pathinfo($fileNameOriginal, PATHINFO_EXTENSION);

                $fileName = $filenameShort . '_' . time() . '.' . $fileExtension;

                $fileSize = round($file->getSize() * 0.0000010, 2);
                Storage::disk('s3')->putFileAs('assets', new File($file->getPathName()), $fileName);
                Storage::disk('s3')->delete('assets/' . $request->file_old);

                //save unity file to local storage folder, to extract it
                Storage::disk('local')->putFileAs('/', new File($file->getPathName()), $fileName);
                $phar = new \PharData(storage_path('app/' . $fileName));
                $phar->extractTo(storage_path('app/' . $fileName . '_dir'));


                //build folders tree and save previews to S3
                $files = $this->getDirContents(storage_path('app/' . $fileName . '_dir'));
                Storage::disk('s3')->deleteDirectory('previews/' . $request->id);

                $foldersArr = [];
                for($i = 0; $i <= count($files) - 1; $i++){
                    $currentPath = explode('/', $files[$i]);

                    if(isset($files[$i - 1])){

                        $prevPath = explode('/', $files[$i - 1]);

                        if($currentPath[7] == $prevPath[7]){
                            if(isset($currentPath[8]) && $currentPath[8] == 'pathname') {
                                $content = file_get_contents($files[$i]);;
                                $foldersArr[ '/' . $content] = '/' . $content;


                                $previewPath = storage_path('app/' . $fileName . '_dir/' . $currentPath[7] . '/preview.png');
                                $previewMaxPath = storage_path('app/' . $fileName . '_dir/' . $currentPath[7] . '/asset');

                                if(file_exists($previewMaxPath) && is_array(getimagesize($previewMaxPath))){
                                    $contentArr = explode('/', $content);
                                    $contentLastEl = array_pop($contentArr);

                                    $newPreviewName = $contentLastEl . '.png';
                                    Storage::disk('s3')->putFileAs('previews/' . $request->id, new File($previewMaxPath), $newPreviewName);
                                }else{
                                    if(file_exists($previewPath)){
                                        $contentArr = explode('/', $content);
                                        $contentLastEl = array_pop($contentArr);

                                        $newPreviewName = $contentLastEl . '.png';
                                        Storage::disk('s3')->putFileAs('previews/' . $request->id, new File($previewPath), $newPreviewName);

                                    }
                                }



                            }
                        }
                    }

                }

                $foldersTreeArr = $this->explodeTree($foldersArr, '/');

//                $this->delFoldersTree(storage_path('app/' . $fileName . '_dir'));
                unlink(storage_path('app/' . $fileName));
            }else{
                $fileName = $request->file_old;
                $fileSize = $request->filesize_old;
                $foldersTreeArr = $request->folders_tree_old;
            }


            $icon = $request->file('icon');
            $icon_1 = $request->file('icon_1');
            $iconData = $this->iconEdit($icon,  $request->icon_old, $request->icon_min_old);
            $iconSecondData = $this->iconEdit($icon_1, $request->icon_old_1, $request->icon_min_old_1);

            DB::table('catalog')->where('id', $request->id)->update(['icon' => $iconData['iconName'],  'icon_1' => $iconSecondData['iconName'], 'icon_min' => $iconData['iconNameMin'], 'icon_min_1' => $iconSecondData['iconNameMin'], 'title' => $request->title, 'description' => $request->description, 'file_name' => $fileName, 'file_size' => $fileSize, 'folders_tree' => $foldersTreeArr]);
            DB::table('catalog_categories')->where('id_catalog', $request->id)->update(['id_category' => $request->category]);

            DB::table('catalog_tags')->where('id_catalog', $request->id)->delete();
            $this->saveTags($request->id, $request->tags);
            return ['status' => 'success', 'msg' => 'Asset saved'];
        }catch (\Exception $e) {
            return ['status' => 'error', 'msg' =>  $e->getMessage()];
        }

    }


    public function iconEdit($icon, $icon_old, $icon_old_min){
        if(!empty($icon)){
            $originalName = $icon->getClientOriginalName();
            $IconOriginalName = pathinfo($originalName, PATHINFO_FILENAME) . '_' . time();
            $extension = pathinfo($originalName, PATHINFO_EXTENSION);
            $iconName = $IconOriginalName  . '.' . $extension;
            $iconNameMin = $IconOriginalName . '-min.' . $extension;

            Storage::disk('s3')->putFileAs('icons', new File($icon->getPathName()), $iconName);

            $image_resize = Image::make($icon->getRealPath());
            $image_resize->resize(150, 150);
            $image_resize->save(public_path('images/' . $iconNameMin));

            Storage::disk('s3')->putFileAs('icons', new File(public_path('images/' . $iconNameMin)), $iconNameMin);
            unlink(public_path('images/' . $iconNameMin));

            if($icon_old != 'no-image-1.png') {
                Storage::disk('s3')->delete('icons/' . $icon_old);
                Storage::disk('s3')->delete('icons/' . $icon_old_min);
            }
        }else{
            $iconName = $icon_old;
            $iconNameMin = $icon_old_min;
        }

        return ['iconName' => $iconName, 'iconNameMin' => $iconNameMin];
    }

    /**
     * Save tags for asset while add or edit
     * @param integer $id - asset id
     * @param string $tagsStr - incomig string of tags
     * @return void
     */
    private function saveTags($id, $tagsStr){
        if(!empty(trim($tagsStr))){
            $tags = explode(',', $tagsStr);
            foreach($tags as $tag){
                $tag = trim(str_replace(['\'', '"'], '', $tag));
                $tagsExists = DB::table('tags')->where('title', $tag)->first();

                if(empty($tagsExists)){
                    $tagId = DB::table('tags')->insertGetId(['title' => $tag]);
                    DB::table('catalog_tags')->insert(['id_catalog' => $id, 'id_tag' => $tagId]);
                }else{
                    DB::table('catalog_tags')->insert(['id_catalog' => $id, 'id_tag' => $tagsExists->id]);
                }
            }
        }

    }


}
