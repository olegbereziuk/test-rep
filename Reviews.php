<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Mixed_;

class Reviews extends Model
{

    protected $table = 'reviews';
    protected $primaryKey = 'id';

    public function getReviewGridData($request){

        $usersQuery = User::where('users.archive', 0)
            ->select(DB::raw('users.id, users.first_name, users.second_name, users.email, users.img, users.position, users.role_id'))
            ->leftJoin('reviews as r', 'r.user_id', '=', 'users.id');

        if(!empty($request->first_name)){
            $usersQuery->where(function($usersQuery) use ($request) {
                $usersQuery->where('users.first_name', 'like', '%' . $request->first_name . '%')
                            ->orWhere('users.second_name', 'like', '%' . $request->first_name . '%');
            });
        }

//        if(!empty($request->first_name)){
//            $usersQuery->where('users.second_name', 'like', '%' . $request->first_name . '%');
//        }


        if(!empty($request->email)){
            $usersQuery->where('users.email', 'like', '%' . $request->email . '%');
        }

        $users = $usersQuery->groupBy('users.id')->get()->toArray();


        $newReviews = DB::table('reviews')
            ->select(DB::raw('count(id) as reviews_new, user_object_id'))
            ->where('status_id', config('data_params.review_status.new'))
            ->groupBy('user_object_id')
            ->get()->toArray();

        $totalReviews = DB::table('reviews')
            ->select(DB::raw('count(id) as reviews_total, user_object_id'))
            ->groupBy('user_object_id')
            ->get()->toArray();


        $lastReviews = DB::select(DB::raw(" SELECT MAX(cast(datetime as datetime)) as datetime, user_object_id FROM reviews 
                                                    WHERE status_id = ? GROUP BY user_object_id"), [config('data_params.review_status.new')]);




        foreach($users as $key => $user)
        {
            $users[$key]['reviews_new'] = 0;
            $users[$key]['reviews_total'] = 0;
            $users[$key]['last_review'] = null;

            foreach($newReviews as $review){
                if($review->user_object_id == $user['id']){
                    $users[$key]['reviews_new'] = $review->reviews_new;
                }
            }

            foreach($lastReviews as $lastReview){
                if($lastReview->user_object_id == $user['id']){
                    $users[$key]['last_review'] = $lastReview->datetime;
                }
            }

            foreach($totalReviews as $totalReview){
                if($totalReview->user_object_id == $user['id']){
                    $users[$key]['reviews_total'] = $totalReview->reviews_total;
                }
            }
        }

        array_multisort(array_column($users, 'reviews_new'), SORT_DESC, array_column($users, 'last_review'), SORT_DESC, $users);

        return json_encode($users);
    }


    public function createNewEmailReviews($request)
    {
        $usersIds = explode(',', $request->all()['users']);

        $newReviews = [];

        foreach($usersIds as $key => $userId){
            $newReviews[$key]['user_id'] = $userId;
            $newReviews[$key]['user_object_id'] = $request->all()['user_object'];
            $newReviews[$key]['status_id'] = config('data_params.review_status.pending');
        }

        DB::table('reviews')->insert($newReviews);

        $userObjectInfo = DB::table('users')->where('id', $request->all()['user_object'])->first();
        $usersInfo = DB::table('users')->whereIn('id', $usersIds)->get()->toArray();
        $this->sendReviewEmail($usersInfo, $userObjectInfo, 'send');
    }


    public function sendReviewEmail(array $usersEmails, $userObjectInfo, string $emailReason){

        try {
            $emailTemplate =  DB::table('additional_settings')->where('alias', 'email_template')->get()->toArray();

            switch($emailReason){
                case 'send':
                    $textTemplate =  DB::table('additional_settings')->where('alias', 'email_review_request')->get()->toArray();
                    break;
                case 'remind':
                    $textTemplate =  DB::table('additional_settings')->where('alias', 'email_review_reminder')->get()->toArray();
                    break;

            }


            $emailText = str_replace('{link}', env('APP_URL') , $textTemplate[0]->value);
            $emailTemplate = str_replace('{title}', 'Review request', $emailTemplate[0]->value);
            $email['pageTemplate'] = str_replace('{text}', $emailText, $emailTemplate);


            foreach($usersEmails as $user) {
                if(!$userObjectInfo){
                    $userObjectInfoIterate = DB::table('users')->where('id', $user->user_object_id)->first();
                    $userObjectName = $userObjectInfoIterate->first_name . ' ' . $userObjectInfoIterate->second_name;
                }else{
                    $userObjectName = $userObjectInfo->first_name . ' '  . $userObjectInfo->second_name;
                }

                $emailTextFinal = str_replace('{reviewsName}',  $userObjectName, $email['pageTemplate']);
                $email['page'] = str_replace('{name}', $user->first_name, $emailTextFinal);
                $user->userObjectName = $userObjectName;

                Mail::send('emails.review_request', $email, function($message) use ($user)
                {
                    $message->subject('Review request about ' . $user->userObjectName);
                    $message->to($user->email);

                });

            }
        }catch(\Exception $e){
            dd($e->getMessage());
        }

    }


    public function getIncomingReviewsByUser($id, array $status = null, int $limit =200)
    {
        if(!$status){
            $status = config('data_params.review_status');
        }


        $data['reviews'] = DB::table('reviews as r')
            ->select(DB::raw('r.id, r.user_id, r.user_object_id, r.feedback, r.to_improve, r.status_id, r.datetime, u.first_name, u.second_name, u.img, rs.title'))
            ->leftJoin('users as u', 'r.user_id', '=', 'u.id')
            ->leftJoin('review_status as rs', 'r.status_id', '=', 'rs.id')
            ->where('r.user_object_id', '=', $id)
            ->whereIn('r.status_id', $status)
            ->orderBy('r.status_id')
            ->orderBy('r.datetime', 'desc')
            ->limit($limit)
            ->get()->toArray();


        $data['user'] = DB::table('users')->where('id', $id)->get()->toArray();
        $data['users'] = DB::table('users')->get()->toArray();
        $data['statuses'] = DB::table('review_status')->orderBy('id')->get()->toArray();

        return $data;
    }


    public function getOutgoingReviewsByUser($id, int $limit =200)
    {

        $data['reviews'] = DB::table('reviews as r')
            ->select(DB::raw('r.id, r.user_object_id, r.feedback, r.to_improve, r.status_id, r.datetime, u.first_name, u.second_name, u.img, rs.title as status'))
            ->leftJoin('users as u', 'r.user_object_id', '=', 'u.id')
            ->leftJoin('review_status as rs', 'r.status_id', '=', 'rs.id')
            ->where('r.user_id', '=', $id)
            ->orderBy('r.datetime', 'desc')
            ->limit($limit)
            ->get()->toArray();

        $data['user'] = DB::table('users')->where('id', $id)->get()->toArray();
        $data['users'] = DB::table('users')->get()->toArray();

        return $data;
    }


}
